//
//  ViewController.m
//  HostKeeper
//
//  Created by alexander.oschepkov on 04.08.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView setHeaderView:nil];
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)openTerminal:(NSButton *)sender {
    NSString *scriptPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"exp"];

    NSString *scriptCommand = [NSString stringWithFormat:@"tell application \"Terminal\"\n activate\n tell application \"System Events\" to keystroke \"t\" using command down\n do script \"%@ aICm5nHNdxhw 93.174.131.171 merkator\" in window 1\n end tell", scriptPath];
    
    NSAppleScript *as = [[NSAppleScript alloc] initWithSource: scriptCommand];
    [as executeAndReturnError:nil];
}



@end
