//
//  main.m
//  HostKeeper
//
//  Created by alexander.oschepkov on 04.08.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
