//
//  ViewController.h
//  HostKeeper
//
//  Created by alexander.oschepkov on 04.08.17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property (weak) IBOutlet NSTableView *tableView;

@end

